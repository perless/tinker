module.exports = {
    title: function (){
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow
    },

    line: function(title = "="){

        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black
    },

    allMale: function(p){
        let n = [];
        for (x of p) {
            if (x.gender === "Male") {
                n.push(x);
            }
        }
        return n;
    },

    allFemale: function(p){
        let n = [];
        for (x of p) {
            if (x.gender === "Female") {
                n.push(x);
            } 
        }
        return n;
    },

    nbOfMale: function(p){
        return this.allMale(p).length;
    },

    nbOfFemale: function(p){
        
        return this.allFemale(p).length;
    },

    nbOfMaleInterest: function(p){
        let n = [];
        for (x of p) {
            if (x.looking_for === "M") {
                n.push(x);
            }
        }
        return n.length;
    },

    nbOfFemaleInterest: function(p){
        let n = [];
        for (x of p) {
            if (x.looking_for === "F") {
                n.push(x);
            }
        }
        return n.length;
    },

    match: function(p){
        return "not implemented".red;
    },

    nbLaKichta: function(p){
        let n = [];
        for (x of p) {
            y = x.income.substring(1);
            if (parseFloat(y) > 2000){
                n.push(x);
            }
        }
        return n.length;
    },
    Movi: function(p) {
        let n = [];
        for (x of p) {
            if (x.pref_movie.includes("Drama")) {
                n.push(x);

            }
        }
        return n.length;
    },

    sf: function(p){
        let n = 0;
        for (let x of this.allFemale(p)) {
            if (x.pref_movie.includes("Sci-Fi")) {
                n++;

            }
        }
        return n;
    },

    doc: function(p){
         let n = 0;
        for(let x of p) {
            if(parseFloat(x.income.substring(1)) > 1482 && x.pref_movie.includes('Documentary')) {
                n++;
            }
        }
            return n;
            
    },
    allRev : function(p){
        let n = [];
        for(let x of p)
     
            if(parseFloat(x.income.substring(1)) > 4000){
             n.push(x.id,x.first_name,x.last_name,x.income);
    }
            return n;

    },
    gMoula: function(p) {
    let riche = 0;
    let array = [];
    for (let x of this.allMale(p)){
        if (parseFloat(x.income.substring(1)) > riche){
            riche = x.income.substring(1);
            array = [x.id, x.last_name];
        };
    };
    return `l'homme le plus riche est ${array}, sont salaire est de ${riche}$`;
},

    salaireMoyen: function(p){
        let total = 0;
        for (let x of p) {
            revenu = parseFloat(x.income.substring(1))
            total += revenu;
        }
        return Math.round(total/p.length);
    },

    salaireMedian: function(p){
        let n = [];
        let tab = p.length;
        let milieu = parseInt(tab/2);
            for (let x of p) {
                let salaire = parseFloat(x.income.substring(1))
                n.push(salaire)
                n.sort((a, b) => a - b)
            };
            return Math.round((n[milieu] +n [milieu+1]) /2);
     },

     nbHemNord: function(p){
         let n = 0;
         for (let x of p) {
             if (x.latitude > 0) {
                 n++;
             }
         }
         return n;
     },

     allOf_HemSud: function(p){
         let perss = [];
         for (let x of p) {
             if(x.latitude < 0) {
                 perss.push(x)
             }
         }
         return perss;
     },
     salaireMoyenHemSud: function(p){
         let sud = this.allOf_HemSud(p);
         return this.salaireMoyen(sud);
     },

     pythagore: function(a, b){
         let x = Math.sqrt(
             ((b.long - a.long)**2) + 
             ((b.lat - a.lat)**2)
             );
         return x; 
     },

     closeToCawt: function(p){
         let a = {long:0, lat:0};
         let grand = 999999999;
         let leplusproche = [];
         for (let i of p){
             let b = {long:i.longitude, lat: i.latitude}
             if(i.last_name == "Cawt"){
                 a = {long:i.longitude, lat:i.latitude}
             }
             if((this.pythagore(a,b) < grand) &&(this.pythagore(a,b) !== 0)){
                 grand = this.pythagore(a,b);
                 leplusproche = [i.last_name, i.id];
             }
         }
         return leplusproche;
     },

     closeToBrach: function(p){
         let a = {long:0, lat:0};
         let grand = 999999999;
         let leplusproche = [];
         for(let i of p){
             let b = {long:i.longitude, lat: i.latitude}

             if(i.last_name == "Brach"){
                 a = {long:i.longitude, lat:i.latitude}
             }
             if((this.pythagore(a,b) < grand) &&(this.pythagore(a,b) !== 0)){
                grand = this.pythagore(a,b);
                leplusproche = [i.last_name, i.id];
         }
     }
     return leplusproche;
    },

    dixPersCloseToBoshard: function(p){
        let dist = [];
        let latBoshard = 57.6938555
        let longBoshard = 11.9704401
        for(let x of p) {
            let a = latBoshard - x.latitude
            let b = longBoshard - x.longitude
            let distance_entre_eux = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
            dist.push({distance : distance_entre_eux, nom : x.last_name, id : x.id } )
            dist.sort((a, b) => a.distance - b.distance)
            }
            
            let lesdix=[]
            for (i=1; i<11; i++){
                lesdix.push(dist[i]);
            }
            return lesdix;
        },

        nameIdGoogle: function(p){
            let list = [];
            const google = p.filter(p => p.email.includes('google'))
                for(let x of google) {
                    list.push({name: x.last_name, id: x.id})
                }
                return list;
            },

            calcAge: function(p) {
                let dob = p.date_of_birth
                let birthday = new Date(dob);
                return ~~((Date.now() - birthday) / 31557600000);
              },

              older: function (p) {
                let people = [];
                for (let x of p) {
                  people.push({ name: x.last_name, age: this.calcAge(x) });
                  people.sort(function (a, b) {
                    return b.age - a.age;
                  });
                }
                return people[0];
              },
            
              younger: function (p) {
                let people = [];
                for (let x of p) {
                  people.push({ name: x.last_name, age: this.calcAge(x) });
                  people.sort(function (a, b) {
                    return a.age - b.age;
                  });
                }
                return people[0];
              },
}